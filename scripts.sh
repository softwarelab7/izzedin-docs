#!/bin/bash

function create-venv() {
	mkvirtualenv izzedin-docs
}

function venv-workon() {
	workon izzedin-docs
}
