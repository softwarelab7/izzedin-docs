
### Changing the Details on Contact Form

1. Log in using Admin credentials.

2. Go to Contact page.

3. Set your page into editing mode by clicking the _**Live**_ status button.

4. Click _**Structure**_.

5. The following plugins are the only plugins that you need to update in order to change the details of your contact page.

	![Contact](/img/con1.png) <br></br>

6. To edit the _**Contact Form**_, move your cursor over its menu and select _**Edit**_, same goes with other concerned plug ins.

	![Contact](/img/con2.png) <br></br>

7. A new window will come out. This is the right time to update the information of your contact form, you can change the form name, email recipient, and even the message on successful submit that your users are gonna receive is editable too.

	![Contact](/img/con3.png) <br></br>

	![Contact](/img/con4.png) <br></br>

8. Click _**Save**_ to continue or _**Cancel**_ to return to the menu. If you made some update, don't forget to click _**Publish Changes**_ to post the made changes. <br></br>




### Changing the Details on Text Template

1. Log in using Admin credentials.

2. Go to Contact page.

3. Set your page into editing mode by clicking the _**Live**_ status button.

4. Click _**Structure**_.

5. To edit the details on **Text** template, simply move your cursor over its menu and select _**Edit**_. This only applies to _**Address**_, _**Number**_ and _**Note**_ plugins. As an example, i will try to edit the information of **Address**.

	![Contact](/img/con6.png) <br></br>

6. A new window will pop out named **Text** , you many now update text or the message that is currently posted on the site.

	![Contact](/img/con5.png) <br></br>

7. Click _**Save**_, wait til the page finish its loading. Click _**Publish Changes**_ afterwards. A message must appear that says 'the content was successfully published' and you must see now the update that you just made. <br></br>



### Changing the Address on Google Map Template

1. Log in using Admin credentials.

2. Go to Contact page.

3. Set your page into editing mode by clicking the  _**Live**_ status button.

4. Click _**Structure**_.

5. To edit the details on **Google Map** template, move your cursor over its menu and select _**Edit**_. This only applies to _**Location Map**_ plugin.

	![Contact](/img/con7.png) <br></br>

7. A new window will pop out named **Google Map**. You can update the **Address**, **Zip Code**, **City** and other related fields if necessary.

	![Contact](/img/con8.png) <br></br>

8. Once you're done, click _**Save**_, wait til the page finish its loading. Click _**Publish Changes**_ afterwards to publish your made changes. A message must appear that says 'the content was successfully published' and the map must direct you now to newly changed address as you clicked **Get Directions** button.

	![Contact](/img/con9.png) <br></br>

	**Note:** If you wish to update the details of your map, you must update the address information as well on **Address** plugin through **Text** template to avoid confusion to end users. <br></br>



### Deleting an Existing Template

1. Log in using Admin credentials.

2. Go to Contact page.

3. Set your page into editing mode by clicking the  _**Live**_ status button.

4. Click _**Structure**_.

5. To delete an existing template, select the template under this page that you wish to delete. Move your cursor over the template's menu and select _**Delete**_. Let's try to remove the existing **Contact Form**

	![Contact](/img/con10.png) <br></br>

6. As you clicked the **Delete** option, a message will pop up to validate your action. Here's the sample message..

	![Contact](/img/con11.png) <br></br>

7. Click **Yes, I'm sure** button to continue or **Cancel** to void your action and return to the menu. When you clicked yes, template will no longer visible under concerned plugin. Click _**Publish Changes**_ button to publish your made changes.


