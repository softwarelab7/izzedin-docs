
### Changing Shop Information

1. Log in using Admin credentials.

2. Click _**Manage**_

	![Shop](/img/cus9.png)

	then click _**Store**_ to edit or add _**Shop Information**_

	![Shop](/img/store.png) <br></br>

3. Fill out the Shop Information

	![Shop](/img/store1.png) <br></br>

4. Click _**Submit**_

	![Shop](/img/store2.png) <br></br>