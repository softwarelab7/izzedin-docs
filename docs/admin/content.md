

### Adding a New Banner Using Frontend Editing

The only way to manage your banner is through django CMS, a content management system platform linked to this site that makes the administrator's job more quick and simple. To get started, observe the following guidelines:

1. Always log in using your Admin credentials.

2. Go to homepage, quickest way to do that is by clicking the Izzedin logo.

	![banner](/img/b1.png) <br></br>

3. Toggle the CMS menu on the page if its not already open:

	![cms_menu](/img/cm1.png)

	To toggle the menu, just click the toggle button on the upper left-most area of the screen.

	![cms_menu](/img/cm2.png)

4. Enable the editing mode of the page, there are two ways to do that:

	- The simplest way is by clicking the _**OFF**_ Edit mode button at the top left hand of the page. Once clicked, it will turned to _**ON**_ status.

 	![banner](/img/b2.png) <br></br>

6. From there, look for _**Banners**_ plugin, hover over the **Banner Container Plugin** row.

	![banner](/img/b13.png)

	This would reveal a menu:

	![banner](/img/b14.png)

	and look for **Add child plugin** > _**Banner Plugin**_. A new window will then come out.

 	![banner](/img/b6.png) <br></br>

7. To upload an image click _**Choose file**_ and locate the picture that you want to display as your banner, click _**Save**_ afterwards.

	![banner](/img/b7.png) <br></br>

8. To post the made changes on main site, click _**Publish**_. Once clicked, notice that _**Edit mode**_ is now back to _**OFF**_, a confirmation message will prompt and voila the image is now posted.

	![banner](/img/b10.png) <br></br>

	![banner](/img/b11.png) <br></br>



### Changing an Existing Banner Using Frontend Editing

1. Log in using Admin credentials.

2. After you signed in, go to homepage, quickest way to do that is by clicking the Izzedin logo.

5. Hover over the existing banner image. Click the pencil button to edit.

	![banner](/img/b9.png) <br></br>

6. _**Picture**_ window will come out, change the existing banner by clicking _**Choose file**_ , locate and upload your preferred new image and click _**Save**_.

7. Make sure to click the _**Publish**_ button to post the made changes. <br></br>



### Deleting an Existing Banner Using Frontend Editing

1. Log in using Admin credentials.

2. After you signed in, go to homepage, quickest way to do that is by clicking the Izzedin logo.

3. Turn your page into editing mode, toggle **Edit mode** to _**ON**_ status button located at the top right hand of the page. Status will turned to **ON** after, that means page is now ready to be modified.

	![banner](/img/b3.png) <br></br>

5. Hover over the existing banner image. Click the encircled X button to delete.

	![banner](/img/b12.png) <br></br>

6. A confirmation message will pop out to validate your action.

7. Click _**OK**_ button to continue or _**Cancel**_ to cancel operation. When _**OK**_ button is clicked, hit the _**Publish**_ button to post the made changes. <br></br>


### Editing About Page

1. Log in using Admin credentials.

2. After you signed in, go to About page, quickest way to do that is by clicking the About.

	![About](/img/about.png) <br></br>

3. Turn your page into editing mode, toggle **Edit mode** to _**ON**_ status button located at the top right hand of the page. Status will turned to **ON** after, that means page is now ready to be modified.

	![About](/img/about1.png) <br></br>

4. Click on _**Add**_ on _**About Content**_.

	![About](/img/about2.png) <br></br>

 	Click on _**Text**_

	![About](/img/about1.png) <br></br>

	You can now add text content to your page. Just type in the content on the text console.

	![footer content](/img/footereditcontent8.png) <br></br>

	If you want to save the changes just click _**Save**_.

	![footer content](/img/footereditcontent9.png) <br></br>

	Click _**Publish**_ to see the changes on your website.

	![footer content](/img/footereditcontent10.png) <br></br>

	![About](/img/about6.png) <br></br>


5. Click on _**Add**_ on _**About Sidebar**_.
	
	![About](/img/about4.png) <br></br>

	![About](/img/about5.png) <br></br>

	Select either you want to add _**Text or Picture**_. You can add text or pictures as many as you want.

	If you want to add Text, Click on _**Text**_

	![About](/img/about1.png) <br></br>

	You can now add text content to your page. Just type in the content on the text console.

	![footer content](/img/footereditcontent8.png) <br></br>

	If you want to save the changes just click _**Save**_. <br></br>

	If you want to add a Picture, Click on _**Picture**_.

	![footer content](/img/about5.png) <br></br>

	Then upload the picture you want to put in by clicking _**Image**_ then choose the file you want up upload. You can also attach a link to your picture, select the page where you want your picture to appear, add alternate text, add description and choose the side where the picture will be put in on the page.

	![footer content](/img/footereditcontent5.png) <br></br>

	If you want to save the changes just click _**Save**_.

	![footer content](/img/footereditcontent6.png) <br></br>

6. Click _**Publish**_ to see the changes on your website.

	![footer content](/img/footereditcontent10.png) <br></br>

	![About](/img/about7.png) <br></br>



### Editing Details on Footer Links

1. Log in using Admin credentials.

2. Turn your page into editing mode, toggle **Edit mode** to _**ON**_ status button located at the top right hand of the page. Status will turned to **ON** after, that means page is now ready to be modified.

	![banner](/img/b3.png) <br></br>

3. Go to the footer link you want to edit on the page, then click on _**Add**_.

	![footer](/img/footeredit.png) <br></br>

	Click on _**Available Plugins**_

	![footer](/img/footeredit1.png) <br></br>

	Click on _**Pencil Icon**_

	![footer](/img/footeredit2.png) <br></br>

4. You will see a form where you can edit the Name of the footer, put a link for the footer, page, add email address, and target window.

	![footer](/img/footeredit3.png) <br></br>

5. Click _**Save**_ to save changes.

	![footer](/img/footeredit4.png) <br></br>



### Editing Footer Contents

1. Log in using Admin credentials.

2. Click on the footer you want to edit contents. Like Terms and Conditions, click on that link. You will be directed to terms and conditions page. 

	![footer content](/img/footereditcontent1.png) <br></br>

3. Turn your page into editing mode, toggle **Edit mode** to _**ON**_ status button located at the top right hand of the page. Status will turned to **ON** after, that means page is now ready to be modified.

	![footer content](/img/footereditcontent1.png) <br></br>

4. Click _**Add**_

	![footer content](/img/footereditcontent2.png) <br></br>

5. Select the plug in you want to add.

	![footer content](/img/footereditcontent3.png) <br></br>

	If you want to add a picture, just click on _**Picture**_

	![footer content](/img/footereditcontent4.png) <br></br>

	Then upload the picture you want to put in by clicking _**Image**_ then choose the file you want up upload. You can also attach a link to your picture, select the page where you want your picture to appear, add alternate text, add description and choose the side where the picture will be put in on the page.

	![footer content](/img/footereditcontent5.png) <br></br>

	If you want to save the changes just click _**Save**_.

	![footer content](/img/footereditcontent6.png) <br></br>

	If you want to add text, just click on _**Text**_

	![footer content](/img/footereditcontent7.png) <br></br>

	You can now add text content to your page. Just type in the content on the text console.

	![footer content](/img/footereditcontent8.png) <br></br>

	If you want to save the changes just click _**Save**_.

	![footer content](/img/footereditcontent9.png) <br></br>

6. Click _**Publish**_ to see the changes on your website.

	![footer content](/img/footereditcontent10.png) <br></br>

	![footer content](/img/footereditcontent11.png) <br></br>



### Changing the Link of Existing Social Media Buttons

1. Log in using Admin credentials.

2. Turn your page into editing mode, toggle **Edit mode** to _**ON**_ status button located at the top right hand of the page. Status will turned to **ON** after, that means page is now ready to be modified.

	![banner](/img/b3.png) <br></br>

4. Hover over any of the social media icons (Facebook/Instagram) menu and select the pencil button to edit.

	![social](/img/sm2.png)<br></br>

5. _**Social Button Plugin**_ will pop out with current value or link per social media account. To change the link, simply override the new link to any of the four existing social media fields.

	![social](/img/sm3.png)<br></br>

6. Click _**Save**_ and then publish the made changes. <br></br>



### Modifying the content on Text Plugin

The text plugin that you can find on CMS has different functions that you can use to modify the content that you made. These functions are located at the upper part of the **Text Plugin** window. Function icons and its usages are listed below:

![text](/img/t1.png) **Undo** & **Redo**- this will undo or redo your last action. <br></br>
![text](/img/t9.png) **Bold**- this applies bold styles to selected text. <br></br>
![text](/img/t10.png) **Italics**- this applies italic styles to selected text. <br></br>
![text](/img/t12.png) **Subscript**- sets the text to be a subscript of other text. <br></br>
![text](/img/t13.png) **Superscript**- sets the text to be a superscript of other text. <br></br>
![text](/img/t18.png) **Insert/Remove Numbered List**- adds or removes numbered list to your page. <br></br>
![text](/img/t19.png) **Insert/Remove Bulleted List**- adds or removes bulleted list to your page. <br></br>
![text](/img/t20.png) **Decrease Indent**- decrease the indent of selected content. <br></br>
![text](/img/t21.png) **Increase Indent**- increase the indent of selected content. <br></br>
![text](/img/t22.png) **Table**- allows you to enter table to your page. <br></br>



### Styling and Writing your own HTML using CMS


One other way to improve the look of your page is by using HTML. HTML or Hypertext Markup Language has a big role in modifying the elements of a site. It consists series of short codes that are called **tags**. Tags are the word between the angle brackets that normally come in pairs, the start tag `<tag>`and the end tag `</tag>`.

Simplest way to style a page is by adding the **style=" "** attribute to an html just like `<div>` tag. `<div>` represents division or section of a page. Common way to add one style property attached to a `<div>` tag looks like this:
	`<div style="property:value;"> </div>`

For example, you want the size of your text to be 10px, the attribute would look like this:
	`<div style="font-size: 10px;">text</div>`

You may also apply more than one property in the style attribute. Simply place a semicolon (;) after your first property and value, then add more property and values every after its semicolons. Let's say you want your text to be 10px and italic, we would write the following:
	`<div style="font-size: 10px; font-style: italic">text</div>`


There are plenty of style property that are very useful in CMS (**Text** only) and they are as follows:

| Property      | Description                     | Possible Values         | Example                  |
|:--------------|:--------------------------------|:------------------------|:-------------------------|
| `font-style` | declares the font style | _normal_ <br /> _italic_ <br /> _oblique_ |`<div style="font-style: italic;"></div>`|
| `font-weight` | declares the font weight, sets how thick or thin characters in text should be displayed | _normal_ <br /> _bold_ <br /> _bolder_ <br /> _lighter_ | `<div style="font-weight: bold;"></div>`|
| `font-size` | declares the size of <br /> the font | refers to lengths (example: _6px_, _8px_, _10px_) | `<div style="font-size: 12px;"></div>`|
| `color` | defines the text color <br /> to be used | name color: _red_, _blue_, _purple_ | `<div style="color: yellow;"></div>` |
| `text-align` | specifies the <br /> alignment of text in <br /> an element | _left_ <br /> _right_ <br /> _center_ <br /> _justify_ | `<div style="text-align: left;"></div>` |
| `margin` | defines the space around elements | specify a margin in px: _12px_, _8px_ | `<div style="margin-top: 5px;"></div>` |

<br />

Aside from style property, you should be familiar with some helpful HTML tags.

| Tag | Definition | Usage |
|:----|:-----------|:------|
| `<p>` | defines a paragraph | automatically add some space or <br /> margin before and after each `<p>` |
| `<h1>` to `<h6>` | heading, `<h1>` creates the largest <br /> headline  while `<h6>` creates the smallest | for headings only |
| `<b>` | creates bold text | emphasizes a word (bold) |
| `<i>` | creates italic text | emphasizes a word (italic) |
| `<ol>` | creates a ordered list, this can be <br /> numerical or alphabetical | works with `<li>` tag |
| `<ul>` | creates a bulleted list | use `<ul>` together with `<li>` <br />  to create unordered list |
| `<li>` | defines a list item | used in ordered list `<ol>` and unordered list `<ul>` |

<br></br>
