
### Order Status

1. Log in using Admin credentials.

2. Click _**Manage**_

	![Orders](/img/cus9.png)

	then _**Orders**_.

	![Orders](/img/Order.png) <br></br>

3. You will be able to see all the orders made online. Order ID, Contact Name, Latest Status, Time of Order, Items, Total, Debit and Credit.

	![Orders](/img/Order1.png) <br></br>

### View Complete Order Details

1. Log in using Admin credentials.

2. Click _**Manage**_

	![Orders](/img/cus9.png)

	then _**Orders**_.

	![Orders](/img/Order.png) <br></br>

3. Click _**Add**_, then _**View Complete Details**_.

	![Orders](/img/Order2.png) <br></br>

	![Orders](/img/Order3.png) <br></br>


### View Order History

1. Log in using Admin credentials.

2. Click _**Manage**_

	![Orders](/img/cus9.png)

	then _**Orders**_.

	![Orders](/img/Order.png) <br></br>

3. Click _**Add**_, then _**View Order History**_.

	![Orders](/img/Order4.png) <br></br>

	![Orders](/img/Order5.png) <br></br>

4. To be able to add notes to the order you can click on _**Add a Status**_

	![Orders](/img/addstat.png) <br></br>

	You will be directed to a form wherein you can note the status of the order.

	![Orders](/img/addstat1.png) <br></br>

	To add the comment or status, click on _**Submit**_.

	![Orders](/img/addstat2.png) <br></br>
