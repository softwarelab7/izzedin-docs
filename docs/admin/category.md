
### Adding a New Category

1. Log in using Admin credentials.

2. Click _**Manage**_

	![customer](/img/cus9.png)

	then _**Categories**_.

	![customer](/img/c1.png) <br></br>

3. You will be then directed to Category Management page, created categories are all listed here. To continue, click _**Add a category**_ button.

	![Category](/img/c2.png) <br></br>

4. Fill in or use all the following required fields:
	- **Name**- category title
	- **Slug**- don't fill in unless you want people to visit the category using a non-conventional name. This will be generated automatically if you don't fill in the field.
	- **Parent**- the parent category. Don't change if you want this on the top-most level.
	- **Active**- if this category should show up on the home page.

	![Category](/img/c3.png) <br></br>

5. Click _**Submit**_ to finish or click _**Products**_ to void the change. Newly created category must now added to Categories list. <br></br>

	![Category](/img/category.png) <br></br>

	You will see the category you created on your home page.

	![Category](/img/category1.png) <br></br>




### Adding a New Child Category

1. Log in using Admin credentials.

2. Click _**Manage**_

	![customer](/img/cus9.png)

	then _**Categories**_.

	![customer](/img/c1.png) <br></br>

3. From the menu, select one category that you wish to create a subctegory. Click **Actions** then select _**Add subcategory**_.

	![Child-Category](/img/cc1.png) <br></br>

4. Same with creating a new category, fill in or use all the following required fields:
	- **Name**- category title
	- **Slug**- don't fill in unless you want people to visit the category using a non-conventional name. This will be generated automatically if you don't fill in the field.
	- **Parent**- the parent category. Don't change if you want this on the top-most level.
	- **Active**- if this category should show up on the home page.

5. Click _**Submit**_ to finish or click _**Products**_ to void the change. Newly created category must now added to Categories list. <br></br>

	Hover your mouse under the category you created for you to be able to see the sub-category or child category you have created.

	![Category](/img/category2.png) <br></br>


### Sorting Categories or Child Categories

1. Log in using Admin credentials.

2. Click _**Manage**_

	![customer](/img/cus9.png)

	then _**Categories**_.

	![customer](/img/c1.png) <br></br>

3. From the menu, select the category or child category that you want to reposition. Drag the item (you must see the broken line before you can move the item) to your target place and drop it. Dragged item should look like this:

	![Sort](/img/sc1.png)

Observe the following guideline on dropping the dragged item:

- To relocate, drag and drop the item unto line with circle on its left tip;

	![Sort](/img/sc2.png) <br></br>

- To nest, drag and drop the item unto bordered item.

	![Sort](/img/sc3.png)

	**Note:** While sorting, some categories with child category sometimes tend to unshow its border line when its sub categories are fully displayed, it is difficult to nest when those borders are not shown. To avoid this, always unexpand or hide the properties of a parent by clicking the small black arrow.

	![Sort](/img/sc4.png) <br></br>



### Editing an Existing Category or Child Category

1. Log in using Admin credentials.

2. Click _**Manage**_

	![customer](/img/cus9.png)

	then _**Categories**_.

	![customer](/img/c1.png) <br></br>

3. From the menu, select one category or child category that you wish to update. Click **Actions**, then select _**Edit**_.

	![Edit](/img/ec1.png) <br></br>


4. Just like creating a category or child category, same form will display. Go to section that you wish to edit, you may review again the description of each field below:
	- **Name**- category title
	- **Slug**- don't fill in unless you want people to visit the category using a non-conventional name. This will be generated automatically if you don't fill in the field.
	- **Parent**- the parent category. Don't change if you want this on the top-most level.
	- **Active**- if this category should show up on the home page.

5. Click _**Submit**_ to finish or click _**Products**_ to void the change. Newly created category must now added to Categories list. <br></br>



### Deleting an Existing Category or Child Category

1. Log in using Admin credentials.

2. Go to _**Dashboard**_, _**Catalogue**_ then select _**Categories**_.

	![Category](/img/c1.png) <br></br>

3. From the menu, choose the category or child category that you wish to delete. Click **Actions**, then select _**Delete**_.

	![Delete](/img/dc1.png) <br></br>

4. A delete confirmation message will come out to validate your action.

5. Click _**YES**_ to continue or _**NO**_ to return to the menu. As you clicked the Delete option, concerned category must now removed from the Categories list and must no longer displayed on the main site. <br></br>

