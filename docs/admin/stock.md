
### Managing Product Alerts

These product alerts were created by customers or page visitors who requested to be notified once their desired item which happen to be out of stock became available again. Registered users will simply click **Notify Me** button to enable the alert while guests will enter their email address and click the same **Notify Me** button.

Signed in users can review (and cancel as well) their made alerts at **Account** menu, under **Product Alerts** section. _Status_ and _creation date_ of requests are also shown on the same page.

![stock](/img/s1.png)

In addition, system admin has the full access and permission to manage these alerts. Follow the step by step procedure on viewing and managing product alerts.

1. Log in using Admin credentials.

2. Click **Dashboard** menu, then **Customers** tab and select _**Stock Alert Requests**_.

	![stock](/img/s2.png) <br></br>

3. **Product Alerts** page will display with the list of all alerts created on the site.

4. To search for a certain alert, you can filter the display of listed alerts according to its status or you can simply enter the name or email address of requestor and click **Search**. Click the **Reset** button to empty out the fields. To understand more of this, see the sample visuals below:

	Select any status from the list and click the **Search** button.

	![stock](/img/s3.png) <br></br>

	Enter either the name or email address of requestor and click the **Search** button.

	![stock](/img/s4.png) <br></br>

5. To update the status of an alert, locate the alert from the list, click its **Actions** button and select _**Edit**_. From **Active** status, you can either cancel or close the alert by selecting the status from the choices and click **Save**.

	![stock](/img/s5.png) <br></br>

6. To delete an existing alert, choose the alert that you wish to be removed. Click its **Actions** button and select _**Delete**_. Information about the alert will display again with a message (at the bottom) asking if you are sure for your action. Click **Delete** to continue or **Cancel** to return to the menu.

	![stock](/img/s6.png) <br></br>



### Managing Low Stock Alerts

If you can still remember the **Low stock threshold** field from [Adding a New Product](./product.md#adding-a-new-product) guideline then I think you'll understand more where these alerts was enabled. You can encounter this field while creating a new product, this form is under **Product Stock** section where you also input your product's cost price and shipping price.

![stock](/img/s7.png) <br></br>

As what mentioned before, the purpose of filling in this field is to create an alert when product stock gets low and reached the number entered on this field. There's a section in the system where you can view and manage all of these pre-created alerts. Simply follow the instructions below:

1. As always, you must log in using your Admin credentials.

2. Click **Dashboard**, **Catalogue** and then select _**Low Stock Alerts**_ from the list.

	![stock](/img/s8.png) <br></br>

3. You will be then directed to **Stock Alerts**page, you can find all of the alerts (regardless of status) under **All Alerts** table.

	![stock](/img/s9.png) <br></br>

4. To filter the displayed alerts, choose one from the given 3 choices (**All**, **Open** and **Closed**). Applicable results will display right away on the table.

	![stock](/img/s10.png)

You can notice the **Update** button per line, this leads you to **Product Management** page and you may use this to update the value of **Low stock threshold**. <br></br>





