
### Adding a New Product

1. Log in using Admin credentials.

2. Click _**Manage**_

	![customer](/img/cus9.png)

	then _**Products**_.

	![customer](/img/p9.png) <br></br>

5. After selecting product type, click _**Add A Product**_ button.

6. You'll be directed to new page then, at **Product Details** section, fill in all the required fields:
	- **SKU** - identifier so that you can keep track of inventory
	- **Category**- category of the product in the site. The product will show up under the category section of the site. If none, refer to [Adding a new Category](./category.md#adding-a-new-category) and learn how to create a new one.
	- **Full Name**- name of product.
	- **Description**- refers to product description. This is the right venue to describe product origin, materials used to build the product, what inspires the designer to create such product and alike statements.
	- **Active**- is the status of the product. This is the basis for showing the product on the site.
	- **Featured**- featured products will appear randomly on the homepage.	- **Featured**- featured products will appear randomly on the homepage.
	- **Price**- price of the product.

	![Product-Details](/img/p4.png)

	![Product-Details](/img/p5.png) <br></br>

7. After filling up and submitting the product details, you will be asked for the weight and dimensions of the product in case you want to show them to customers.

	![Product-Details](/img/p10.png) <br></br>

8. At **Product Images**, click _**Add Images**_ and locate the photo of your product that you want to be posted.

	![Product-Image](/img/p7.png) <br></br>

	On the manage product side you should see something like below.

	![Product-Image](/img/product.png) <br></br>

	On the webpage itself, you should see the product.

	![Product-Image](/img/product1.png) <br></br>

	If the product is a featured product, you will see the product picture under where it is categorized on the web.

	![Product-Image](/img/product4.png) <br></br>


### Adding Options to Products

1. Log in using Admin credentials.

2. Click _**Manage**_

	![customer](/img/cus9.png)

	then _**Products**_.

	![customer](/img/p9.png) <br></br>

3. Select the product you want to add options, then Click on _**Actions**_
	
	![Product-Image](/img/productoption.png) <br></br>

4. Click _**Select Options**_

	![Product-Image](/img/productionoption1.png) <br></br>

5. To create an Option for sizes or colors or variations, fill out the Option Category on _**Option Group**_	

	![Product-Image](/img/Puttinginoptions.png) <br></br>

	Fill out the Option/s content to _**Option**_.

	![Product-Image](/img/Puttinginoptions1.png) <br></br>
	
	If there is a price change in selecting from the original to one of the options, the _**Additional Price**_ can be added in _**Price Change**_.

	![Product-Image](/img/Puttinginoptions2.png) <br></br>

	To add another Option category, just click on the _**Plus sign icon**_ beside the Option Group.

	![Product-Image](/img/Puttinginoptions4.png) <br></br>

6. To save the additional Options for the product, just click _**Submit**_.

	![Product-Image](/img/Puttinginoptions3.png) <br></br>

	You should see this page after clicking submit.

	![Product-Image](/img/product2.png) <br></br>

	On the webpage you should see the options

	![Product-Image](/img/product3.png) <br></br>


	
### Editing Product Information

1. Log in using Admin credentials.

2. Click _**Manage**_

	![customer](/img/cus9.png)

	then _**Products**_.

	![customer](/img/productoption.png) <br></br>

3. Select the product you want to edit product information, Click on _**Actions**_ then click on _**Edit Product Information**_.

	![Product-Image](/img/editproductinfo.png) <br></br>

4. You can now _**edit**_ the product information. You can change the product's Full Name, SKU, Description of Product, Active product or Featured product, Change the category it belongs to, Sizing Chart and the Price.

	![Product-Image](/img/Editinfo.png) <br></br>

5. To save the changes for product information, just click _**Submit**_.

	![Product-Image](/img/Editinfo2.png) <br></br>



### Editing Product Dimensions

1. Log in using Admin credentials.

2. Click _**Manage**_

	![customer](/img/cus9.png)

	then _**Products**_.

	![customer](/img/productoption.png) <br></br>

3. Select the product you want to edit product information, Click on _**Actions**_ then click on _**Edit Product Dimensions**_.

	![Product-Image](/img/editdimensions.png) <br></br>


4. You can now _**edit**_ the product dimensions. You can change the product's weight, weight units, length, width, height and dimension units.

	![Product-Image](/img/editdimensions1.png) <br></br>


5. To save the changes for product dimensions, just click _**Submit**_.

	![Product-Image](/img/editdimensions2.png) <br></br>


### Editing Product Images

1. Log in using Admin credentials.

2. Click _**Manage**_

	![customer](/img/cus9.png)

	then _**Products**_.

	![customer](/img/productoption.png) <br></br>

3. Select the product you want to edit product information, Click on _**Actions**_ then click on _**Edit Product Images**_.

	![Product-Image](/img/editproductpic.png) <br></br>

4. You can now _**Add/Remove**_ picture/s of your product. <br></br>

	To add a picture or pictures click on _**Add images**_.

	![Product-Image](/img/editproductpic1.png) <br></br>

	To remove a picture click on _**Delete**_.

	![Product-Image](/img/editproductpic2.png) <br></br>

	The added or deleted pictures will be seen on the page.

	![Product-Image](/img/product5.png) <br></br>


### Deleting a Product

1. Log in using Admin credentials.

2. Click _**Manage**_

	![customer](/img/cus9.png)

	then _**Products**_.

	![customer](/img/productoption.png) <br></br>

3. Select the product you want to Delete, then Click on _**Actions**_ and click on _**Delete**_.

	![Product-Image](/img/deleteprod.png) <br></br>

4. Confirm deleting the product by clicking _**YES**_.

	![Product-Image](/img/deleteprod1.png) <br></br>


**NOTE**

Product/s can only be _DELETED_ if there are no buyer/s or no order/s made online by any customer/s. Once there is an existing or previous order for the product, the system will give you an error message. This is to ensure that all inventory are well accounted for.

![Product-Image](/img/deleteprod2.png) <br></br>