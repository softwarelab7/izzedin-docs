
### Giving Admin Access to a User

1. Log in using Admin credentials.

2. Click _**Manage**_

	![customer](/img/cus9.png)

	then _**Users**_.

	![customer](/img/cus1.png) <br></br>

3. You will be then directed to **Users** page wherein all registered users and their informations were listed. Here's what the page looks like:

	![customer](/img/cus2.png) <br></br>

4. To give an admin access to a user, locate the row of the user and then toggle the **Actions** menu. Click on the **Set as Store Admin** menu option.

	![customer](/img/cus10.png) <br></br>

6. A message will appear that indicates if the operation completed. Look at the users' columns and see that their **Store Admin** status has now a check mark.

	![customer](/img/cus4.png) <br></br>


### Viewing Customer Information

1. Log in using Admin credentials.

2. Click _**Manage**_

	![customer](/img/cus9.png)

	then _**Users**_.

	![customer](/img/cus1.png) <br></br>

3. You will be then directed to **Customer** page wherein all registered users and their informations were listed. Here's what the page looks like:

	![customer](/img/cus2.png) <br></br>

4. To view contact info,

	![customer](/img/cus5.png) <br></br>

5. You will be presented with the details about the contact.

	![customer](/img/cus8.png) <br></br>
