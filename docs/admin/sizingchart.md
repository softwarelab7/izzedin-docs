
### Adding Sizing Chart

1. Log in using Admin credentials.

2. Click _**Manage**_

	![Sizing Chart](/img/cus9.png)

	Then click _**Sizing Chart**_.

	![Sizing Chart](/img/sizingchart.png) <br></br>

3. Click _**Add a Sizing Chart**_

	![Sizing Chart](/img/sizingchart1.png)

	You can now add Sizing chart title and the HTML codes for the sizing chart are auto generated, you can just edit it and put in the sizes you want to put in, or you can just edit it out with a new table using HTML codes.

	![Sizing Chart](/img/sizingchart2.png)

4. Click _**Preview**_ to see the chart preview.
	
	![Sizing Chart](/img/sizingchart4.png)<br></br>

5. When satisfied, click _**Submit**_

	![Sizing Chart](/img/sizingchart3.png)<br></br>

	To see the sizing chart, you may go to the product where you put in the sizing chart and click sizing chart.

	![Sizing Chart](/img/product6.png)<br></br>

	The table will be seen, just like below.

	![Sizing Chart](/img/product7.png)<br></br>



### Editing Sizing Chart

1. Log in using Admin credentials.

2. Click _**Manage**_

	![Sizing Chart](/img/cus9.png)

	Then click _**Sizing Chart**_.

	![Sizing Chart](/img/sizingchart.png) <br></br>

3. Click _**Actions**_ beside the sizing chart you want to edit.

	![Sizing Chart](/img/sizingchart5.png) <br></br>

	Then click _**Edit Sizing Chart**_

	![Sizing Chart](/img/sizingchart6.png) <br></br>

	You may now edit the sizing chart, title and HTML Code.

	![Sizing Chart](/img/sizingchart7.png) <br></br>

4. Click _**Submit**_ to save changes.

	![Sizing Chart](/img/sizingchart8.png) <br></br>


### Delete Sizing Chart

1. Log in using Admin credentials.

2. Click _**Manage**_

	![Sizing Chart](/img/cus9.png)

	Then click _**Sizing Chart**_.

	![Sizing Chart](/img/sizingchart.png) <br></br>

3. Click _**Actions**_ beside the sizing chart you want to delete.

	![Sizing Chart](/img/sizingchart5.png) <br></br>

	Click _**Delete**_

	![Sizing Chart](/img/sizingchart9.png) <br></br>

4. Click _**Yes**_ to delete the chart or click _**No**_ if you don't want to delete the chart.

	![Sizing Chart](/img/sizingchart10.png) <br></br>
