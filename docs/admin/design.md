
### Changing the Image of a Category Link

Currently, Design Services already has its own page that displays an image and links to each category pages when clicked. To change the photo of the said image liks, simply follow the following guidelines.

1. As always, log in using Admin credentials. <br></br>

2. Click **Design Services**, from the two dislayed categoried (Interior and Graphic) select one. Let's try **Interior**. Currently, I haven't change the picture so it displays the default image 'Image Not Available'.

	![ds](/img/ds25.png) <br></br>

3. Items under clicked category will display (if any), but if there isn't added new page yet then it would be a blank page. To make sure that you're in the right page, check the url. Name of the selected category should be the last word in the line, just like this for Interior: <br></br>
**_jawda-staging.softwarelab7.com/design-services/interiors/_** <br></br>

4. Next, click _**Page**_ and select _**Edit This Page**_.

	![ds](/img/ds1.png) <br></br>

5. Click _**Page**_ again and select _**Page Preview**_.

	![ds](/img/ds2.png) <br></br>

6. To change the image, click _**Choose File**_ and select the photo that you want to upload.

	![ds](/img/ds3.png) <br></br>

7. Once done, click _**Save**_ and hit _**Publish Changes**_ button to post your made changes. I uploaded a photo under **Interior** and here's what it looks like now after I publish the change.

	![ds](/img/ds26.png) <br></br>



### Selecting the Right Template

Design Services page displays three different common templates namely: **2 per row links to subpages** (applied to categories such as _Interior_ and _Graphic_), **3 per row links to subpages** (applied to sub categories that contains projects, this displays a title as well) and lastly **full width (no sidebars)** (applied to project page, please refer to [Enabling Thumbnails and Image Preview on Project Page](./design.md#enabling-thumbnails-and-image-preview-on-project-page). On this section, I'll guide you on how to change the template of a page and when to use each respective temmplates.

1. First, log in using your Admin credentials. <br></br>

2. Click **Design Services** from menu. Let's try to set a template that displays 2 items per row, let's put it under **Commercial**. Select Commercial, make sure that the url looks like this: _**jawda-staging.softwarelab7.com/design-services/interiors/commercial/**_. <br></br>

3. Click _**Page**_ and select _**Edit This Page**_.

	![ds](/img/ds1.png) <br></br>

4. Click _**Page**_ again and select _**Templates**_. From then, a list of templates will display, select **2 PER ROW LINKS TO SUBPAGES**.

	![ds](/img/ds27.png) <br></br>

5. Click **Publish Changes** button. Now that we already set our template, let's start to add pages under Commercial. Follow the steps from [Adding a New Child Page](./design.md#adding-a-new-child-page), but put the new pages under **Commercial** instead. Try to add 3 new pages, use any word that you like to name those pages. <br></br>

6. As you publish the three added pages, the selected template of _**2 per row links to subpages**_ should take place, third item should be placed on the second row just like this:

	![ds](/img/ds28.png) <br></br>

	Same goes with _**3 per row links to subpages**_, page should have the following format instead: 3 items per row and has a title at the top left hand of the page.

	![ds](/img/ds29.png) <br></br>



### Enabling Thumbnails and Image Preview on Project Page

As requested, there would be a new page format that displays set of thumbnails (4 columns, 3 rows) on the left section of the page and has an image preview on the right. This preview has the ability to tweet, pin, mail and share on facebook the clicked image. Enabling the said option is so easy, simply follow the steps below:

1. First, log in using your Admin credentials. <br></br>

2. Click **Design Services** and select one project page. Currently, **Residential** has three present projects, let's try to add items on _**River Oaks Project**_. Don't forget to check the url, name of the selected project should be the last word in the line: _**jawda-staging.softwarelab7.com/design-services/interiors/residential/river-oaks-project/**_. <br></br>

3. Let us now set the page template. Click _**Page**_ and select _**Edit This Page**_.

	![ds](/img/ds1.png) <br></br>

4. Click _**Page**_ again and select _**Templates**_. From then, select **FULL WIDTH (NO SIDEBARS)**. 

	![ds](/img/ds30.png)

	To check if you selected the right template, click again _**Page**_ and hover your cursor over _**Templates**_, current selected template is bolder than others.

	![ds](/img/ds42.png) <br></br>

5. Click **Structure** button. Go to **Main** placeholder, drag your cursor over its setting and then locate _**Project Plugin**_ from the list. Click **Save** afterwards.

	![ds](/img/ds31.png) <br></br>

6. As you saved, _**Project Plugin**_ should be now added under **Main**. Let us now add the images under **River Oaks Project**, drag your cursor over the plugin's setting and then select _**Project Image Plugin**_.

	![ds](/img/ds32.png)

	A window will pop out, click _**Choose File**_ button and locate the image that you wish to upload. To add more, repeat step #6 until you reached your desired number of image per project. Click _**Save**_ afterwards. <br></br>

7. Newly uploaded images should be now added under _**Project Plugin**_. As an example, I uploaded 3 new images and that reflects on this picture:

	![ds](/img/ds33.png)

	You may as well click the **Content** button to see the visual version of it. <br></br>

8. Lastly, don't forget to click **Publish Changes** button so that you can save and post the changes that you just made. Here's what the _**River Oaks Project**_ looks like now, observe the link that diverts to **Residential** page when clicked.

	![ds](/img/ds34.png) <br></br>

**Note:** Page will automatically turn to a different format when you uploaded more than 3 images. Concerned images will automatically turn into thumbnails (4 columns, 3 rows max), and clicked thumbnail will display on the right section of the page. On this page, you'll also be able to tweet, pin, mail and share on facebook the clicked thumbnail.

![ds](/img/ds35.png) <br></br>



### Changing the Image of an Existing Thumbnail

Modifying the image displayed on a project page is so easy, simply follow these guidelines.

1. Log in using Admin credentials. <br></br>

2. Go to **Design Services** and locate the image that you wish to change. Let's try to change one of the thumbnails of _**River Oaks Project**_. <br></br>

3. Click the _**Live**_ button, located at the top right hand of the page. As you clicked the button, status will turn to _**Draft**_.

	![ds](/img/ds38.png) <br></br>

4. As page turns turns to _**Draft**_ status, two buttons will show up. Click **Structure**. 

	![ds](/img/ds39.png) <br></br>

5. **Banners** and **Main** plugins will display as well as the **Project Image Plugin** under it, focus on _**Main**_ plugin only. From the list of image plugins under **Project Plugin**, select the plugin that contains the image that you wish to modify. Move your cursor over its setting (at the end of the line) and select _**Edit**_. In this case, I selected **Project Image Plugin 408**.

	![ds](/img/ds36.png) <br></br>

6. **Project Image Plugin** window will pop out, change the image by clicking the _**Choose File**_ button. Select your desired image to replace the existing one and click _**Save**_. 

	![ds](/img/ds40.png) <br></br>

7. After page finish the image uploading, you may click the _**Content**_ button to check if the image is successfully changed. After checking, you'll notice the new blue button named **Publish Changes**. Click the said button to post your just made changes. 

	![ds](/img/ds41.png) <br></br>

	You'll be notified afterwards that the content was successfully published. Particular thumbnail is now replaced with a new image, see below:

	![ds](/img/ds37.png) <br></br>



### Adding a New Child Page

1. Log in using Admin credentials.

2. Click the _**Live**_ button at the top right hand of the page. As you clicked, status will turn to _**Draft**_.

3. Click _**django CMS**_ beside **JAWDAANDJAWDA.COM**.

	![ds](/img/ds7.png) <br></br>

4. Page tree of the site will display. All of published and unpublished (draft) pages are displayed here and to identify the status of your page, simply refer to **EN** column. These circles represents whether your page is published or not. Green means _**published**_, blinking blue means _**unpublished changes**_ and grey means _**unpublished**_ (new page, no previous changes at all).

	![ds](/img/ds8.png)

	To change the status of your page from **Unpublished Changes**/**Unpublished** to **Published**, simply click the blue or grey circle. It will lead you to its full main page of the selected item and from there you'll find the _**Publish Changes**_ or _**Publish Page Now**_ button at the top right side of the page. Click the said button and voila, your page is now published. Let us now proceed to next step to continue in adding a new child page. <br></br>

5. To add a child page, click the _**add child**_ or the green plus icon of the page you desired to have a child page. Let's try to add a new child page under _*Interior**_.

	![ds](/img/ds9.png) 

	There will be 3 new arrow buttons that will appear along the line of your selected page. Hover over the arrows to see its labels:    
	- **Insert above**: means create a page (another sub category) above Interior  
	- **Insert below**: means create a page (another sub category) below Interior  
	- **Insert inside**: means create a child page under Interior 

	Use the approriate arrow according to your need, in this case we want to add a child page to **Interior** so we should choose the _**Insert inside**_ arrow.

	![ds](/img/ds10.png) <br></br>

6. **Add Page** window will pop out. Create the page, minimum requirement is _**Title**_. As you enter the title of your child page, _**Slug**_ will be automatically filled. Click _**Save**_ afterwards. I named our page _**Sample**_.

7. Newly created page is now listed under _**Interior**_. As what discussed in #4, newly created pages are considered as unpublished pages and you can identify that by checking the color of its circle under **EN** column. **Sample** page has gray circle meaning page isn't visible yet on site. To make the page visible and accessible, follow the instructions given on #4. Click the gray circle, wait til it divert you to main page and from then click _**Publish Page Now**_ button. 

	![ds](/img/ds43.png) 

	Return to page tree, click again **django CMS**. **Sample** page should have a published status or green circle by now just like this:

	![ds](/img/ds44.png) 

**Important:** You need to make sure that your newly created or modified page should always be published subsequently so that visitors of the site can access or view at least your made changes. <br></br>


### Editing an Existing Page

1. Log in using Admin credentials.

2. Click _**JAWDAANDJAWDA.COM**_ and select _**Pages..**_.

3. Expand the page tree window to clearly view the list of live pages.

4. Go to certain page under Design Services that you wish to edit, let's choose **Interiors** as an example.

5. To edit the page, click the 'paper with a pencil' icon. Other action icons such as **copy** (two paper icon) and **cut**(paper with a scissor icon) are also useful on this page.

	![ds](/img/ds13.png) <br></br>

6. **Change Page** window will appear, you may now update the details of your page.

7. Click _**Save**_ to continue, a 'change' confirmation message of your action will prompt at the top left hand of the page.

	![ds](/img/ds15.png) <br></br>

8. To finish, close the page tree window to return to main site, a 'publish' confirmation message must display afterwards (see the screenshot below).

	![ds](/img/ds16.png) <br></br>



### Deleting an Existing Page

1. Log in using Admin credentials.

2. Click _**JAWDAANDJAWDA.COM**_ and select _**Pages..**_.

3. Expand the page tree window to clearly view the list of live pages.

4. Go to certain page under Design Services that you wish to remove, let's choose **Hospitality** as an example.

5. To delete the page, click the red 'multiply sign' icon.

	![ds](/img/ds14.png) <br></br>

6. A confirmation message will pop out to validate your action.

7. Click **Yes, I'm sure** button to continue. Observe that Hospitality is no longer listed in the list and a 'delete' confirmation message of your action will prompt at the top left hand of the page.

	![ds](/img/ds17.png) <br></br>

8. To finish, return the page to **Live** page and check Design Services page, **Hospitality** should no longer listed under Interiors category. <br></br>



### Editing an Image

1. Log in using Admin credentials.

	Currently, when you click Interiors category, **Commercial** doesn't have image preview yet, the one displays is the default image 'Image Not Available'.

	![ds](/img/ds0.png) <br></br>

2. Go to _**Design Services**_ page and select one property from the listed categories that displays an image preview (except FAQ's and Our Approach of course). Let's try _**Commercial**_. <br></br>

3. It is still a blank page because there isn't added new page yet under Commercial but you can check the url to be sure that you're directed to your preferred page. We selected Commercial, so it should be now _**jawda-staging.softwarelab7.com/design-services/interiors/commercial/**_. <br></br>

4. Click _**Page**_ and select _**Edit This Page**_.

	![ds](/img/ds1.png) <br></br>

4. Click _**Page**_ again and select _**Page Preview**_.

	![ds](/img/ds2.png) <br></br>

5. To add a preview image, click _**Choose File**_ and select the image that you want to upload.

	![ds](/img/ds3.png) <br></br>


6. Once done, click _**Save**_ and hit _**Publish Changes**_ to post the changes. I uploaded a photo under **Commercial** and here's what it looks like now after I publish the change.

	![ds](/img/ds11.png) <br></br>



### Adding a Text on Our Approach or FAQ Page

1. Log in using Admin credentials.

2. Go to _**Design Services**_, select either Our Approach or FAQ category (text-based pages). Let's try to add text to_**Our Approach**_.

3. Set your page into editing mode by clicking the _**Live**_ button and click _**Structure**_.

4. _**Title**_ and _**Content**_ plugins will appear.

	![ds](/img/ds12.png) <br></br>

	- **Title**- holds the title of the page, we are at **Our Approach** page so it is now named to it.
	- **Content**- holds the body or content of the page.

	Currently Our Approach have no content yet so let's try to add text to Content plugin. Hover its menu and locate _**Text**_.

	![ds](/img/ds4.png) <br></br>

5. At _**Text**_ window, enter the words that you want to be posted on _**Our Approach**_ page. Same goes with _**FAQ**_ page.

	![ds](/img/ds5.png) <br></br>

6. Click _**Save**_ and hit _**Publish Changes**_ to upload the changes. <br></br>



### Editing a Text on Our Approach or FAQ Page

1. Log in using Admin credentials.

2. Go to either **Our Approach** or **FAQs** page. This time, I chose to edit the Our Approach page.

3. Turn your page into editing mode, move your cursor unto existing text. As you can see, there's a writing that says **double click to edit**.

	![ds](/img/ds6.png) <br></br>

4. To edit, double click from the page and then update the content when _**Text**_ window already pop out.

5. Click _**Save**_ and hit _**Publish Changes**_ to upload the changes. <br></br>



### Deleting a Text Entry on Our Approach or FAQ Page

1. Log in using Admin credentials.

2. Go to either **Our Approach** or **FAQs** page. Let's delete the text entry of _**FAQs**_ page as an example.

3. Set your page into editing mode by clicking the _**Live**_ button and click _**Structure**_.

4. Under _**Content**_ plugin, click the menu of the existing text entry and select _**Delete**_.

	![ds](/img/ds18.png) <br></br>

5. A confirmation message will pop out to validate your action.

6. Click **Yes, I'm sure** button to continue and hit _**Publish Changes**_ to upload the changes.

7. A 'publish' confirmation message must appear and content of FAQs page must now again emptied out.

	![ds](/img/ds19.png) <br></br>



### Change the Title of an Existing Category

If you want to update the title of an existing category like turning the word _Interiors_ to _Interior_ (without the letter s), then you're in the right place. On this guideline, I'll teach you to edit the name of current categories under **Design Services** menu. To learn more, refer to the following instructions:

1. First of course, log in on the side using your Admin credentials.

2. Click **JAWDAANDJAWDA.COM** and select _**Pages...**_.

	![ds](/img/ds20.png) <br></br>

3. Page tree of the site will appear on the side page, click the square icon to expand the display of window.

	![ds](/img/ds8.png) <br></br>

4. Window will expand, you now have clearer view of the page tree. To show all the pages, click the plus(+) sign beside **Home**. From then, go to **Design Services** and locate the category that you wish to update the title and click the edit icon (the one with pencil in it).

	![ds](/img/ds21.png)

	![ds](/img/ds22.png) <br></br>

5. Do your thing, update the name of the category, remove the s from _Interiors_. For every change that you made, always click the **Save** button to make sure it's saved successfully.

6. A message will appear that says _'The page "name of page" was changed successfully'_. Notice that category **Interior** is now changed on the page tree.

	![ds](/img/ds23.png) <br></br>

7. Close the window and go back to main site by clicking the multiply(x) icon. Click the **Live** status button located at the upper right hand side of the page. Wait til it changed to **Draft** status, click **Publish Changes** button (blue one) to successfully upload your just made changes. There it goes, _Interiors_ is now changed to _**Interior**_.

	![ds](/img/ds24.png) <br></br>
