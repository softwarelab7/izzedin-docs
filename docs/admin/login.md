
### Logging In

* Go to Izzedin site.

* Click _**Login**_.

	![Sign In](/img/h1.png) <br></br>

* Enter credentials and click _**Login**_.

  	![Log In](/img/h2.png)