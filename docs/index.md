# Welcome to Izzedin Site Docs
This is the documentation on how to manage the site Izzedin.


## Administration Guide

* [Getting Started](admin/login)
	* [Logging In](admin/login) <br></br>

* [Content Management](admin/content)
	* [Adding a New Banner](admin/content#adding-a-new-banner-using-frontend-editing)
	* [Changing an Existing Banner](admin/content#changing-an-existing-banner-using-frontend-editing)
	* [Deleting an Existing Banner](admin/content#deleting-an-existing-banner-using-frontend-editing)
	* [Editing About Page](admin/content#editing-about-page)
	* [Editing Details on Footer Links](admin/content#editing-details-on-footer-links)
	* [Editing Footer Contents](admin/content#editing-footer-contents)
	* [Changing the Link of Existing Social Media Buttons](admin/content#changing-the-link-of-existing-social-media-buttons)
	* [Modifying the content on Text Plugin](admin/content#modifying-the-content-on-text-plugin)
	* [Styling and Writing your own HTML using CMS](admin/content#styling-and-writing-your-own-html-using-cms) <br></br>


* [User Management](admin/customer)
	* [Giving Admin Access to a User](admin/customer#giving-admin-access-to-a-user)
	* [Viewing Customer Information](admin/customer#viewing-customer-information) <br></br>

* [Category Management](admin/category)
	* [Adding a New Category](admin/category#adding-a-new-category)
	* [Adding a New Child Category](admin/category#adding-a-new-child-category)
	* [Sorting Categories or Child Categories](admin/category#sorting-categories-or-child-categories)
	* [Editing an Existing Category or Child Category](admin/category#editing-an-existing-category-or-child-category)
	* [Viewing the Category or Child Category on Site](admin/category#viewing-the-category-or-child-category-on-site)
	* [Deleting an Existing Category or Child Category](admin/category#deleting-an-existing-category-or-child-category)
	* [How to add 'Add to Cart' Buttons on Category Page](admin/category#how-to-add-add-to-cart-buttons-on-category-page)
	* [How to add Product to new Sub-Categories](admin/category#how-to-add-product-to-new-sub-categories) <br></br>

* [Product Management](admin/product)
	* [Adding a New Product](admin/product#adding-a-new-product)
	* [Adding Product Options](admin/product#adding-options-to-product)
	* [Editing a Product](admin/product#editing-product-information)
	* [Editing a Product](admin/product#editing-product-dimensions)
	* [Editing a Product](admin/product#editing-product-images)
	* [Deleting a Product](admin/product#deleting-a-product)


* [Sizing Chart Management](admin/sizingchart)
	*[Adding Sizing Chart](admin/sizingchart#adding-sizing-chart)
	*[Editing Sizing Chart](admin/sizingchart#editing-sizing-chart)
	*[Deleting Sizing Chart](admin/sizingchart#deleting-sizing-chart)


* [Order Management](admin/order)
	* [View Order Status](admin/order#view-order-status)
	* [View Complete Order Details](admin/order#view-complete-order-details)
	* [View Order History](admin/order#view-order-history)

* [Shop Management](admin/shop)